﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPFDataGridToolTip
{
    class Data
    {
        public int No { get; set; }
        public string Value { get; set; }
    }

    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            // データグリッドにテストデータを設定します。
            var list = new List<Data>();
            for (int i = 0; i < 30; ++i)
            {
                var data = new Data();
                data.No = i;
                data.Value = $"Value{i}";
                list.Add(data);
            }
            dataGrid.ItemsSource = list;
        }

        /**
         * @brief データグリッドでマウスが移動した時に呼び出されます。
         * 
         * @param [in] sender データグリッド
         * @param [in] e マウスイベント
         */
        private void DataGrid_MouseMove(object sender, MouseEventArgs e)
        {
            // マウス位置のデータグリッドのセルを取得します。
            var dataGrid = sender as DataGrid;
            var point = e.GetPosition(dataGrid);
            var cell = GetDataGridCell<TextBlock>(dataGrid, point);

            // セルが取得できない場合
            if (cell == null)
            {
                // 終了します。
                return;
            }

            // セルのツールチップを設定します。
            cell.ToolTip = cell.Text;
        }

        /**
         * @brief データグリッドのセルを取得します。
         * 
         * @param [in] dataGrid データグリッド
         * @param [in] point データグリッド上の位置
         */
        private T GetDataGridCell<T>(DataGrid dataGrid, Point point)
        {
            // データグリッド上の指定位置のオブジェクトを取得します。
            T result = default(T);
            var hitResultTest = VisualTreeHelper.HitTest(dataGrid, point);

            // オブジェクトを取得できた場合
            if (hitResultTest != null)
            {
                // Visualオブジェクトを取得します。
                var visualHit = hitResultTest.VisualHit;

                // Visualオブジェクトが取得できる間繰り返します。
                while (visualHit != null)
                {
                    // 取得したVisualオブジェクトが指定した型の場合
                    if (visualHit is T)
                    {
                        result = (T)(object)visualHit;
                        break;
                    }

                    // 親のオブジェクトを取得します。
                    visualHit = VisualTreeHelper.GetParent(visualHit);
                }
            }
            return result;
        }
    }
}
